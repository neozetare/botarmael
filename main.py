import os
import time
from typing import NoReturn

import discord
from dotenv import load_dotenv

import nicks_dealer

RENICK_PERIOD_S: int = 60

load_dotenv()

try:
	CLIENT_TOKEN: str = os.environ['CLIENT_TOKEN']
	TARGET_GUILD_ID: int = int(os.environ['TARGET_GUILD_ID'])
	TARGET_USER_ID: int = int(os.environ['TARGET_USER_ID'])
except KeyError as e:
	raise RuntimeError(f'missing environment variable {e!s}')

intents: discord.Intents = discord.Intents.default()

client = discord.Client(intents=intents)


@client.event
async def on_ready():
	print(f'logged in as {client.user!r}')

	target_guild: discord.Guild = await client.fetch_guild(TARGET_GUILD_ID)
	target_member: discord.Member = await target_guild.fetch_member(TARGET_USER_ID)

	print(f'target member retrieved: {target_member!r}')

	print('starting renick target member loop')
	await start_renick_target_member_loop(target_member)


async def start_renick_target_member_loop(target_member: discord.Member) -> NoReturn:
	while True:
		await renick_target_member(target_member)
		time.sleep(RENICK_PERIOD_S)


async def renick_target_member(target_member: discord.Member) -> None:
	new_nick: str = nicks_dealer.get_random_one()
	await target_member.edit(nick=new_nick)
	print(f'target member renicked as {new_nick!r}')


client.run(CLIENT_TOKEN)
