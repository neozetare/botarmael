# Botar'maël

A simple Discord bot to rename a Discord member (specific user of a specific server) with pseudo random pun nicknames based on the Breton first name "Kar'maël".
Those combine the suffix "ar'maël" with prefixes that ends with the sound "ar" like "Canar'maël" or "Décevoir'maël".

## How to use

- Clone the repository
- Install the dependencies (see `requirements.txt`)
- Add your environment variables (an exemple can be found in the file `.env.example`)
- Start the file `main.py` with Python 3.11

## How to customize prefixes

Prefixes are dealt in the module `nicks_dealer.py`. Add custom code here (that may be based on custom lexicons that you can put in `data/lexicon`) to add prefixes to the module variable `prefixes`.

## To-do

- [FIX] Make renick loop unblocking
- [FEATURE] Add other prefixes:
  - French public figures and other real-life proper nouns
  - Proper nouns from fictional cultural works
- [FEATURE] Implement commands:
  - Change auto renick period
  - Start and stop auto renick
  - Perform one renick now

## Licence

This project is under the Do What The Fuck You Want To Public License Version 2 (WTFPLv2).

See `license.txt` for more informations.

### Attribution

- [python-dotenv](https://github.com/theskumar/python-dotenv) is under BSD License
- [discord.py](https://github.com/Rapptz/discord.py) is under MIT License
- [Lexique382](http://openlexicon.fr/datasets-info/Lexique382/README-Lexique.html) is under CC BY-SA 4.0