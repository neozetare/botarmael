import csv
import random

SUFFIX: str = '\'maël'

MAX_NICK_SIZE: int = 32
MAX_PREFIX_SIZE: int = MAX_NICK_SIZE - len(SUFFIX)

SILENT_ENDS: list[str] = [
	'rrent',
	'rrhes',
	'rent',
	'rres',
	'rrhe',
	'res',
	'rre',
	'rds',
	'rts',
	'rcs',
	're',
	'rs',
	'rd',
	'rt',
	'rc',
	'rr',
	'r',
]

prefixes: set[str] = set()


def normalize_prefix(prefix: str) -> str:
	for silent_end in SILENT_ENDS:
		if prefix.endswith(silent_end):
			return prefix.removesuffix(silent_end) + 'r'

	raise RuntimeError(f'unable to normalize the prefix {prefix!r}')


with open('data/lexicon/Lexique382.tsv', encoding='utf-8') as file:
	reader = csv.DictReader(file, delimiter='\t', strict=True)

	for line in reader:
		# Ignore words that sound like "car" or "arrhes"
		if line['phon'] in ('aR', 'kaR'):
			continue

		# Ignore words that don't end with the sound "ar"
		if not line['phon'].endswith('aR'):
			continue

		prefix = normalize_prefix(line['ortho'])
		assert prefix.endswith('ar') or prefix.endswith('oir')

		# Ignore prefixes that are too big
		if len(prefix) > MAX_PREFIX_SIZE:
			continue

		prefixes.add(prefix)


def get_random_one():
	nick = random.choice(list(prefixes)) + SUFFIX
	return nick[0].upper() + nick[1:]
